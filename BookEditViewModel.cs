﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_24._04._22_WpfApp1
{
    public class BookEditViewModel : INotifyPropertyChanged
    {
        private string imgEdit;

        public event PropertyChangedEventHandler PropertyChanged;

        public string NameEdit { get; set; }
        public DateTime DateTime { get; set; }
        public string ImgEdit
        {
            get => imgEdit;
            set
            {
                imgEdit = value;

                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImgEdit)));
            }
        }
        public List<Author> Authors { get; set; }

        protected bool SetProperty<T>(ref T field, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (!Equals(field, newValue))
            {
                field = newValue;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                return true;
            }

            return false;
        }

        private DateTime? dateEdit;

        public DateTime? DateEdit { get => dateEdit; set => SetProperty(ref dateEdit, value); }
    }
}
