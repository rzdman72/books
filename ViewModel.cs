﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.ObjectModel;

namespace Csharp_24._04._22_WpfApp1
{
    public class ViewModel
    {
        public ViewModel()
        {
            Books.Add(new Book()
            {
                Id=1,
                Name="Сказка о рыбаке и рыбке (новое издание)",
                Date=new DateTime(2001,01,01),
                Author= Authors.First(),
                Image = "https://web-skazki.ru/24a36e8b-8fc3-4e74-8ac9-2a6315964f29/preview-files/skazka-o-rybake-i-rybke-x-768.jpg"
            });

            Books.Add(new Book()
            {
                Id = 2,
                Name = "Война и мир!",
                Date = new DateTime(1956, 01, 01),
                Author = Authors.Skip(1).First(),
                Image = "https://cdn.ast.ru/v2/ASE000000000710292/COVER/cover1__w340.jpg"
            });

            Books.Add(new Book()
            {
                Id = 3,
                Name = "Cказка о царе Салтане",
                Date = new DateTime(1986, 01, 01),
                Author = Authors.First(),
                Image = "https://upload.wikimedia.org/wikipedia/commons/8/8d/V.N._Kurdyumov_-_Tale_of_Tsar_Saltan_%281913%29_00_cover.jpg"
            });
        }

        public void DeleteBook(ulong bookId)
        {
            var book = Books.FirstOrDefault(x => x.Id == bookId);
            if (book == null) return;

            Books.Remove(book);
        }

        public void DeleteBook(Book book)
        {
            if (book == null) return;
            Books.Remove(book);
        }
        public ObservableCollection<Book> Books { get; set; } = new ObservableCollection<Book>();
        
        public ObservableCollection<Author> Authors { get; set; } = new ObservableCollection<Author>
        {
                new Author()
                {
                    Id = 10,
                    Name= "Александр",
                    LastName="Сергеевич",
                    SurName= "Пушкин"
                },
                new Author()
                {
                    Id = 20,
                    Name= "Лев",
                    LastName="Николаевич",
                    SurName= "Толстой"
                },
        };
    }
    public class Author
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }

        public string AuthorFormated => $"{Name} {LastName} {SurName}";
    }
    public class Book
    {
        public ulong Id { get; set; }
        public int Id_author { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Cover { get; set; }
        public Author Author { get; set; }
        public string Image { get; set; }

        public string DateFormated => Date.ToString("dd.MM.yyyy");
    }
}
